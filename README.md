# MsgParserRestApi
REST Api service for parsing .msg file in json array using Spring Boot
____

### How to run:
 1. Open command line in the project directory and enter 'mvn spring-boot:run'
 
### How to use:
 1. Create POST request to http://localhost:8080/api/msg-parameters
 2. Add to body parameter 'file' with file using .msg extension 
 3. Add to body int parameter 'msgType'. You can parse 3 types of message:
     - **horizontal table message**  - msgType = 1
     - **vertical table message** - msgType = 2
     - **plain text message**  - msgType = 3
 4. Send request