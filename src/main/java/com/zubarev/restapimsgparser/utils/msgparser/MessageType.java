package com.zubarev.restapimsgparser.utils.msgparser;

public enum MessageType {
    HORIZONTAL_TABLE, VERTICAL_TABLE, PLAIN_TEXT
}
