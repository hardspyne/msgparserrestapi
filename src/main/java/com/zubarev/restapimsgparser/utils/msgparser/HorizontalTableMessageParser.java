package com.zubarev.restapimsgparser.utils.msgparser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
class HorizontalTableMessageParser extends MessageParser {

    @Value("${horizontal_table.parameters}")
    String parametersFieldName;

    HorizontalTableMessageParser() {
    }

    Map<String, String> parseMsg(InputStream inputStream) throws IOException {
        Document document = Jsoup.parse(inputStream, "utf-8", "");

        List<Element> rowsWithParameters = document.getElementsByTag("tr")
                .stream()
                .filter(element -> element.children()
                        .hasAttr("align")).collect(Collectors.toList());

        List<Element> parametersNames = rowsWithParameters.get(0).children();
        List<Element> parametersValues = rowsWithParameters.get(1).children();

        return IntStream.range(0, parametersNames.size())
                .boxed()
                .collect(Collectors.toMap(i -> parametersNames.get(i).text(),
                        i -> parametersValues.get(i).text().trim(), (a, b) -> b));
    }

    @Override
    String[] getParametersFieldNames() {
        return parametersFieldName.split(", ");
    }
}
