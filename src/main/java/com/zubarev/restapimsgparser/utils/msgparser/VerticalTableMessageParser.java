package com.zubarev.restapimsgparser.utils.msgparser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
class VerticalTableMessageParser extends MessageParser {

    @Value("${vertical_table.parameters}")
    String parametersFieldName;

    VerticalTableMessageParser() {
    }

    Map<String, String> parseMsg(InputStream inputStream) throws IOException {
        Document document = Jsoup.parse(inputStream, "utf-8", "");

        List<Element> parametersNames = document.getElementsByTag("b");
        List<Element> parametersValues = document.getElementsByAttributeValue("style", "padding: 13px 10px; width: 70%;");

        return IntStream.range(0, parametersNames.size())
                .boxed()
                .collect(Collectors.toMap(i -> parametersNames.get(i).text(),
                        i -> parametersValues.get(i).text().trim(), (a, b) -> b));
    }

    @Override
    String[] getParametersFieldNames() {
        return parametersFieldName.split(", ");
    }
}
