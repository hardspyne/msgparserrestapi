package com.zubarev.restapimsgparser.utils.msgparser;

import com.auxilii.msgparser.MsgParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
class PlainTextMessageParser extends MessageParser {

    @Value("${plain_text.parameters}")
    private String parametersFieldName;

    PlainTextMessageParser() {
    }

    Map<String, String> parseMsg(InputStream inputStream) throws IOException {
        String messageText = new MsgParser().parseMsg(inputStream).getBodyText();

        String[] lines = messageText.split(System.getProperty("line.separator"));
        //get lines with only one ':'
        List<String> parameterLines = Arrays.stream(lines)
                .filter(s -> s.contains(":"))
                .collect(Collectors.toList());

        parameterLines.removeIf(s -> s.indexOf(":") != s.lastIndexOf(":"));

        return parameterLines.stream()
                .map(parameterLine -> parameterLine.split(":"))
                .collect(Collectors.toMap(split -> split[0], split -> split[1].trim(), (a, b) -> b));
    }

    @Override
    String[] getParametersFieldNames() {
        return parametersFieldName.split(", ");
    }
}
