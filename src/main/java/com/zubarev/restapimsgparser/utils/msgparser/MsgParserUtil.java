package com.zubarev.restapimsgparser.utils.msgparser;

import com.zubarev.restapimsgparser.exceptions.UnknownLetterTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class MsgParserUtil {

    @Autowired
    private HorizontalTableMessageParser horizontalTableMessageParser;
    @Autowired
    private VerticalTableMessageParser verticalTableMessageParser;
    @Autowired
    private PlainTextMessageParser plainTextMessageParser;

    @Value("${msg_parameters.print_form}")
    private String parametersPrintName;

    public List<String> parseMsg(int msgType, InputStream inputStream) throws IOException, UnknownLetterTypeException {

        if (msgType < 0 || msgType > MessageType.values().length) {
            throw new UnknownLetterTypeException("unknown letter type");
        } else {
            MessageType messageType = MessageType.values()[msgType - 1];
            return getParametersList(messageType, inputStream);
        }
    }

    private List<String> getParametersList(MessageType messageType, InputStream inputStream) throws IOException {
        List<String> parametersList = new ArrayList<>();

        switch (messageType) {
            case HORIZONTAL_TABLE:
                parametersList = getParametersListFromParametersMap(inputStream, horizontalTableMessageParser);
                break;
            case VERTICAL_TABLE:
                parametersList = getParametersListFromParametersMap(inputStream, verticalTableMessageParser);
                break;
            case PLAIN_TEXT:
                parametersList = getParametersListFromParametersMap(inputStream, plainTextMessageParser);
        }
        inputStream.close();

        return parametersList;
    }

    private List<String> getParametersListFromParametersMap(InputStream in, MessageParser msgParser) throws IOException {
        Map<String, String> parametersMap = msgParser.parseMsg(in);
        String[] parameterPrintNames = parametersPrintName.split(", ");
        String[] parametersFieldNames = msgParser.getParametersFieldNames();

        return IntStream.range(0, parameterPrintNames.length)
                .mapToObj(i -> parameterPrintNames[i] + ": " + parametersMap.get(parametersFieldNames[i]))
                .collect(Collectors.toList());
    }
}
