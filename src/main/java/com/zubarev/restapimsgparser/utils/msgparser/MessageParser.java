package com.zubarev.restapimsgparser.utils.msgparser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public abstract class MessageParser {

    abstract Map<String, String> parseMsg(InputStream inputStream) throws IOException;

    abstract String[] getParametersFieldNames();
}
