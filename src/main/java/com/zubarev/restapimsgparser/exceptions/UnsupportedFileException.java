package com.zubarev.restapimsgparser.exceptions;

public class UnsupportedFileException extends Exception {
    public UnsupportedFileException() {
        super();
    }

    public UnsupportedFileException(String message) {
        super(message);
    }

    public UnsupportedFileException(String message, Throwable cause) {
        super(message, cause);
    }
}
