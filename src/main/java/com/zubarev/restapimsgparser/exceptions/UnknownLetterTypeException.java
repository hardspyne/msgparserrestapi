package com.zubarev.restapimsgparser.exceptions;

public class UnknownLetterTypeException extends Exception{
    public UnknownLetterTypeException() {
        super();
    }

    public UnknownLetterTypeException(String message) {
        super(message);
    }

    public UnknownLetterTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
