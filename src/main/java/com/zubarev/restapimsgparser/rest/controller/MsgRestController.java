package com.zubarev.restapimsgparser.rest.controller;

import com.zubarev.restapimsgparser.exceptions.UnsupportedFileException;
import com.zubarev.restapimsgparser.utils.msgparser.MsgParserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MsgRestController {

    @Autowired
    private MsgParserUtil msgParser;

    @PostMapping("/msg-parameters")
    public List<String> getMsgParameters(@RequestParam("msgType") int msgType,
                                         @RequestParam("file") MultipartFile file) throws Exception {

        if (file.getContentType() == null || !file.getContentType().endsWith("vnd.ms-outlook")) {
            throw new UnsupportedFileException("file is not msg");
        }
        return msgParser.parseMsg(msgType, file.getInputStream());
    }
}


