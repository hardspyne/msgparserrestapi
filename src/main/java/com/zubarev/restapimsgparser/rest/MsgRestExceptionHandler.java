package com.zubarev.restapimsgparser.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class MsgRestExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<MsgParserErrorResponse> handleException(HttpRequestMethodNotSupportedException exc) {
        MsgParserErrorResponse messageParserErrorResponse = new MsgParserErrorResponse(
                404, exc.getMessage(), System.currentTimeMillis());

        return new ResponseEntity<>(messageParserErrorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<MsgParserErrorResponse> handleException(NoHandlerFoundException exc) {
        MsgParserErrorResponse messageParserErrorResponse = new MsgParserErrorResponse(
                404, exc.getMessage(), System.currentTimeMillis());

        return new ResponseEntity<>(messageParserErrorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<MsgParserErrorResponse> handleException(Exception exc) {
        MsgParserErrorResponse messageParserErrorResponse = new MsgParserErrorResponse(
                400, exc.getMessage(), System.currentTimeMillis());

        return new ResponseEntity<>(messageParserErrorResponse, HttpStatus.BAD_REQUEST);
    }
}
